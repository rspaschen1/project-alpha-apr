from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import CreateProjectForm


@login_required
def projects_view(request):
    projects = Project.objects.filter(owner=request.user)
    return render(
        request, "projects/list_projects.html", {"projects": projects}
    )


@login_required
def project_detail(request, id):
    project = get_object_or_404(Project, id=id)
    return render(
        request, "projects/project_detail.html", {"project": project}
    )


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    return render(request, "projects/create_project.html", {"form": form})
