from django.urls import path
from tasks.views import create_task, tasks_view

urlpatterns = [
    path("mine/", tasks_view, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
]
