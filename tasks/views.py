from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from tasks.forms import CreateTaskForm


@login_required
def tasks_view(request):
    tasks = Task.objects.filter(assignee=request.user)
    return render(request, "tasks/list_tasks.html", {"tasks": tasks})


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    return render(request, "tasks/create_task.html", {"form": form})
